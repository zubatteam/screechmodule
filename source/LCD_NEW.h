/******************************************************************************
  Projeto..: Elevador V3
  Arquivo..: LCD_NEW.h
  Vinculos.: (SEM VINCULO)
  Fun��o...: Biblioteca de comunica��o para display 16x2
  Autores..: Genisson R. Albuquerque
             Edson Felipe C. Benevides
  Vers�o...: 2013.06.15
  Licen�a..: Copyright 2012
  Descri��o: Fun��es de interface para display de LCD 16x2
*******************************************************************************/

/******************************************************************************
Constantes
*******************************************************************************/

#define LCD_ENABLE   DISPLAY_E
#define LCD_RW       DISPLAY_RW
#define LCD_RS       DISPLAY_RS
#define LCD_DATA0    DISPLAY_DATA0
#define LCD_DATA1    DISPLAY_DATA1
#define LCD_DATA2    DISPLAY_DATA2
#define LCD_DATA3    DISPLAY_DATA3
#define LCD_TYPE     2    // 0=5x7, 1=5x10, 2=2 linhas
#define LCD_LINE_TWO 0x40 // Endereco da LCD RAM para a segunda linha

/******************************************************************************
Variaveis globais
*******************************************************************************/

BYTE const LCD_INIT_STRING[4] = {0x20 | (lcd_type << 2), 0xc, 1, 6}; // Estes bytes precisam serem enviados para o LCD para inicia-lo

/******************************************************************************
Prototypes
*******************************************************************************/

BYTE LCD_READ_BYTE();
void LCD_SEND_NIBBLE(BYTE Parte4);
void LCD_SEND_BYTE(BYTE Addres, BYTE Caract);
void LCD_INIT();
void LCD_GOTOXY(BYTE PosicX, BYTE posicY);
void LCD_PUTC(char Caract);
char LCD_GETC(BYTE PosicX, BYTE PosicY);
void LCD_CLEAR_LINE(int NumLin);