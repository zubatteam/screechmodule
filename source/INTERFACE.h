/******************************************************************************
  Projeto..: Elevador V3
  Arquivo..: INTERFACE.h
  Vinculos.: (SEM ARQUIVO VINCULADO)
  Função...: Interface para as EDs e definições de constantes de configuração
  Autores..: Genisson R. Albuquerque
             Edson Felipe C. Benevides
  Versão...: 2013.06.18
  Licença..: Copyright 2013
  Descrição: Define as constantes utilizadas para configurar o PIC e o MCP2515,
             alem de vincular os pinos do PIC as suas respectivas EDs.
*******************************************************************************/

/******************************************************************************
Constantes de configurações gerais
*******************************************************************************/

/******************************************************************************
Diretivas de compilação
*******************************************************************************/

/******************************************************************************
Registradores do PIC
*******************************************************************************/

/******************************************************************************
Variaveis/Definições de Configuração da Porta
*******************************************************************************/


/******************************************************************************
Configurações da CAN
*******************************************************************************/


/******************************************************************************
Configurações dos Agendamentos (Tempos)
*******************************************************************************/

/******************************************************************************
Configurações dos PINOS do PIC
*******************************************************************************/

#define CAN_RX                   PIN_B3
#define CAN_TX                   PIN_B2
#define BOTAO_DIMINUIR           PIN_B5
#define BOTAO_ALMENTAR           PIN_B1
#define BOTAO_ENTER              PIN_B7
#define BOTAO_VOLTAR             PIN_B6
#define BOTAO_PROG               PIN_B0
#define DISPLAY_E                PIN_E0
#define DISPLAY_RW               PIN_E1
#define DISPLAY_RS               PIN_E2
#define DISPLAY_BACKLIGHT        PIN_B4
#define DISPLAY_DATA0            PIN_A5
#define DISPLAY_DATA1            PIN_A3
#define DISPLAY_DATA2            PIN_A2
#define DISPLAY_DATA3            PIN_A1
#define ENTRADA_ANTI_ESMAGAMENTO PIN_C3
#define SAIDA_ANTI_ESMAGAMENTO   PIN_D1
#define SENSOR_PRESENCA          PIN_C2
#define ENTRADA_RESERVA1         PIN_C1
#define ENTRADA_RESERVA2         PIN_C0
#define LUZ_E_VENTILACAO         PIN_D7
#define VENTILACAO               PIN_D4
#define RELE_RESERVA1            PIN_D6
#define RELE_RESERVA2            PIN_D5
#define HABILITA_PORTA           PIN_D3
