/******************************************************************************
  Projeto..: Screech
  Arquivo..: MAIN.c
  Autores..: Edson Benevides
			 Raphael Mendes
  Vers�o...: 0.1
  Descri��o: Modulo para comunicacao do sensor com o microcontrolador
*******************************************************************************/

#include "18f4580.h"
#device HIGH_INTS=TRUE, ADC=10
#use DELAY(CLOCK=20000000, RESTART_WDT)
#fuses HS, NOWDT, NOPROTECT, NOWRTB, PUT, NOBROWNOUT, NOMCLR
#use fast_io(C)
/******************************************************************************
Includes
*******************************************************************************/
#include "INTERFACE.h"
#include "LCD_NEW.h"
#include "LCD_NEW.c"
/******************************************************************************
Nome......: main
Fun��o....: Fun��o principal do programa
Parametros: (SEM PARAMETROS)
Retorno...: (SEM RETORNO)
*******************************************************************************/
#bit  RBIF = 0xFF2.0

boolean edge;
long int time;

#INT_CCP1
void ccp_isr(){
	if(!edge)
	{
		SET_TIMER1(0); 
		setup_ccp1(CCP_CAPTURE_FE);
		edge = !edge;
	}
	else
	{	
		time =	(long int)GET_TIMER1()*0.2/58;
		setup_ccp1(CCP_CAPTURE_RE);
		edge = !edge;	
	}
	
	LCD_PUTC('\f');
	printf(LCD_PUTC,"%li cm", time );
	delay_ms(500); 	
}


void main(){
	LCD_INIT();
	LCD_PUTC('\f');
	SET_TRIS_B(0b01000000);
	edge = INPUT(PIN_C2);
	setup_timer_1( T1_INTERNAL | T1_DIV_BY_1);
	enable_interrupts(GLOBAL);
	enable_interrupts(INT_CCP1);	
	setup_ccp1(CCP_CAPTURE_RE);  
	while(TRUE){
	 	printf(LCD_PUTC,"BEM - VINDO");
		delay_ms(200); 
		LCD_PUTC('.');
		delay_ms(50); 
		LCD_PUTC('.');
		delay_ms(50); 
		LCD_PUTC('.');
		delay_ms(50); 			
		LCD_PUTC('\f'); 
	}
} 
