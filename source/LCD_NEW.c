/******************************************************************************
  Projeto..: Elevador V3
  Arquivo..: LCD_NEW.c
  Vinculos.: LCD_NEW.h
  Fun��o...: Biblioteca de comunica��o para display 16x2
  Autores..: Genisson R. Albuquerque
             Edson Felipe C. Benevides
  Vers�o...: 2013.06.15
  Licen�a..: Copyright 2012
  Descri��o: Fun��es de interface para display de LCD 16x2
*******************************************************************************/

/******************************************************************************
Nome......: LCD_READ_BYTE
Descricao.: Ler dados do LCD
Parametros: (SEM PARAMETROS)
Retorno...: Retorna o byte lido
*******************************************************************************/
BYTE LCD_READ_BYTE(){
   BYTE Readed;
   OUTPUT_FLOAT(LCD_DATA0);
   OUTPUT_FLOAT(LCD_DATA1);
   OUTPUT_FLOAT(LCD_DATA2);
   OUTPUT_FLOAT(LCD_DATA3);
   OUTPUT_HIGH(LCD_RW);
   delay_cycles(1);
   OUTPUT_HIGH(LCD_ENABLE);
   delay_cycles(1);
   Readed = 0;
   Readed = (INPUT(LCD_DATA3)<<7)|(INPUT(LCD_DATA2)<<6)|(INPUT(LCD_DATA1)<<5)|(INPUT(LCD_DATA0)<<4);
   OUTPUT_LOW(LCD_ENABLE);
   delay_cycles(1);
   OUTPUT_HIGH(LCD_ENABLE);
   delay_us(1);
   Readed = (INPUT(LCD_DATA3)<<3)|(INPUT(LCD_DATA2)<<2)|(INPUT(LCD_DATA1)<<1)|INPUT(LCD_DATA0);
   OUTPUT_LOW(LCD_ENABLE);
   OUTPUT_LOW(LCD_DATA0);
   OUTPUT_LOW(LCD_DATA1);
   OUTPUT_LOW(LCD_DATA2);
   OUTPUT_LOW(LCD_DATA3);
   return(Readed);
}

/******************************************************************************
Nome......: LCD_SEND_NIBBLE
Descricao.: Envia um peda�o de um byte (4 bits)
Parametros: <BYTE Parte4> Os 4 bits que se deseja enviar
Retorno...: (SEM RETORNO)
*******************************************************************************/
void LCD_SEND_NIBBLE(BYTE Parte4){
   OUTPUT_BIT(LCD_DATA0, bit_test(Parte4, 0));
   OUTPUT_BIT(LCD_DATA1, bit_test(Parte4, 1));
   OUTPUT_BIT(LCD_DATA2, bit_test(Parte4, 2));
   OUTPUT_BIT(LCD_DATA3, bit_test(Parte4, 3));
   delay_cycles(1);
   OUTPUT_HIGH(LCD_ENABLE);
   delay_us(2);
   OUTPUT_LOW(LCD_ENABLE);
}

/******************************************************************************
Nome......: LCD_SEND_BYTE
Descricao.: Envia um byte/caractere para o display (em duas partes de 4 bits)
Parametros: <BYTE Addres> Endereco do caractere no display
            <BYTE Caract> Caractere ou byte a ser enviado
Retorno...: (SEM RETORNO)
*******************************************************************************/
void LCD_SEND_BYTE(BYTE Addres, BYTE Caract){
   OUTPUT_LOW(LCD_RS);
   while(bit_test(LCD_READ_BYTE(), 7));
   OUTPUT_BIT(LCD_RS, Addres);
   delay_cycles(1);
   OUTPUT_LOW(LCD_RW);
   delay_cycles(1);
   OUTPUT_LOW(LCD_ENABLE);
   LCD_SEND_NIBBLE(Caract >> 4);
   LCD_SEND_NIBBLE(Caract & 0xf);
}

/******************************************************************************
Nome......: LCD_INIT
Descricao.: Inicializa o LCD
Parametros: (SEM PARAMETROS)
Retorno...: (SEM RETORNO)
*******************************************************************************/
void LCD_INIT(){
   BYTE PonFor;
   OUTPUT_LOW(LCD_DATA0);
   OUTPUT_LOW(LCD_DATA1);
   OUTPUT_LOW(LCD_DATA2);
   OUTPUT_LOW(LCD_DATA3);
   OUTPUT_LOW(LCD_RS);
   OUTPUT_LOW(LCD_RW);
   OUTPUT_LOW(LCD_ENABLE);
   delay_ms(15);
   for(PonFor = 1; PonFor <= 3; ++PonFor){
      LCD_SEND_NIBBLE(3);
      delay_ms(5);
   }
   LCD_SEND_NIBBLE(2);
   for(PonFor = 0; PonFor <= 3; ++PonFor){
      LCD_SEND_BYTE(0, LCD_INIT_STRING[PonFor]);
   }
   output_high(DISPLAY_BACKLIGHT);
}

/******************************************************************************
Nome......: LCD_GOTOXY
Descricao.: Coloca o ponteiro do display em uma determinada posicao
Parametros: <BYTE PosicX> Coluna
            <BYTE posicY> Linha
Retorno...: (SEM RETORNO)
*******************************************************************************/
void LCD_GOTOXY(BYTE PosicX, BYTE posicY){
   BYTE Addres;

   if(PosicY != 1){
      Addres = LCD_LINE_TWO;
   }else{
      Addres = 0;
   }
   Addres += PosicX-1;
   LCD_SEND_BYTE(0,0x80|Addres);
}

/******************************************************************************
Nome......: LCD_PUTC
Descricao.: Envia um caractere para o display (utilizado na printf)
Parametros: <char Caract> Caractere a ser enviado
Retorno...: (SEM RETORNO)
*******************************************************************************/
void LCD_PUTC(char Caract){
   switch (Caract) {
      case '\f': LCD_SEND_BYTE(0, 1);
                 delay_ms(2);
                 LCD_GOTOXY(1, 1);         break;
      case '\n': LCD_GOTOXY(1, 2);         break;
      case '\b': LCD_SEND_BYTE(0, 0x10);   break;
      default  : LCD_SEND_BYTE(1, Caract); break;
   }
}

/******************************************************************************
Nome......: LCD_GOTOXY
Descricao.: Ler o caractere em uma determinada posicao do display
Parametros: <BYTE PosicX> Coluna
            <BYTE posicY> Linha
Retorno...: Retorna o caractere lido
*******************************************************************************/
char LCD_GETC(BYTE PosicX, BYTE PosicY){
   char Caract;

    LCD_GOTOXY(PosicX, PosicY);
    while(bit_test(LCD_READ_BYTE(), 7)); // Aguarda ate o sinal de ocupado estar desativado
    OUTPUT_HIGH(LCD_RS);
    Caract = LCD_READ_BYTE();
    OUTPUT_LOW(LCD_RS);
    return(Caract);
}

/******************************************************************************
Nome......: LCD_CLEAR_LINE
Descricao.: Limpa uma linha e coloca o curso na mesma
Parametros: <int NumLin> Numero da linha
Retorno...: (SEM RETORNO)
*******************************************************************************/
void LCD_CLEAR_LINE(int NumLin){
   LCD_GOTOXY(1,NumLin);
   printf(LCD_PUTC, "                ");
   LCD_GOTOXY(1,NumLin);
}